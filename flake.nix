{
  description = "A fast and beautiful NixOS and Nix management utility";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
    poetry.url = "github:nix-community/poetry2nix";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    poetry,
    ...
  }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [poetry.overlay];
      };
      inherit (pkgs) poetry2nix;
      nxEnv = poetry2nix.mkPoetryEnv {
        projectDir = ./.;
        preferWheels = true;
      };
    in rec {
      packages.nx = poetry2nix.mkPoetryApplication {
        projectDir = ./.;
        python = pkgs.python310;
        preferWheels = true;
      };

      packages.default = packages.nx;

      apps.nx = utils.lib.mkApp {
        drv = packages.nx;
      };

      apps.default = apps.nx;

      devShells.default = nxEnv.env.overrideAttrs (oldAttrs: {
        buildInputs = [pkgs.python310Packages.black pkgs.python310Packages.poetry];
      });

      formatter = pkgs.alejandra;
    });
}
