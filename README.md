# NX 

A fast and beautiful NixOS and Nix management utility ❄


## 💡 Features

- Beautiful and powerful CLI using Python's [typer](https://typer.tiangolo.com/) library
- Quick and easy deployment and management of NixOS systems and Nix profiles


## 💾 Installation

### 💽 From Binary

**Install NX using `nix profile`**
```bash
  $ nix profile install gitlab:mchal_/nx
  $ nx
```

## 📸 Screenshots

![NX Screenshot](https://media.discordapp.net/attachments/1014738924515635302/1046919833339568208/image.png)

## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)
